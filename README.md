# Requirements
`pip install plotly pandas`

# Usage
usage: relation_chart.exe [-h] gdp_file tariff_file

Draw chart from CSV file

positional arguments:
  gdp_file     CSV file containing gdp
  tariff_file  CSV file containing tariffs

optional arguments:
  -h, --help   show this help message and exit