#!/usr/bin/env python3

import plotly.express as px
import argparse
import random
import csv


# Global variables
CSV_HEADER_LINES = 4
CSV_HEADER_STRUCTURE = ["Country Name", "Country Code", "2017"]


# Project functions
def parsing_arguments():
    parser = argparse.ArgumentParser(description="Draw chart from CSV file")
    parser.add_argument('gdp_file', help='CSV file containing gdp')
    parser.add_argument('tariff_file', help='CSV file containing tariffs')
    args = parser.parse_args()

    return args


def check_data_structure(structure):
    if len(CSV_HEADER_STRUCTURE) != len(structure):
        print("Wrong csv file header structure")
        exit(84)

    for index, value in enumerate(CSV_HEADER_STRUCTURE):
        if value != structure[index]:
            print("Wrong csv file header structure")
            exit(84)


def csv_to_data(file):
    f = open(file, "r+")
    for index in range(CSV_HEADER_LINES):
        f.readline()
    structure = list(csv.reader([f.readline().strip('\n')], skipinitialspace=True))[0]
    data_list = []

    check_data_structure(structure)
    for line in f:
        data = {}
        raw_data = list(csv.reader([line.strip('\n')], skipinitialspace=True))[0]
        for index, name in enumerate(structure):
            data[name] = raw_data[index]
        data_list.append(data)

    return data_list


def parse_data(data_list):
    obj = {}

    for elem in data_list[0].keys():
        obj[elem] = []

    for elem in data_list:
        for key in elem.keys():
            if elem[key] != '' and not isinstance(elem[key], str):
                obj[key].append(float(elem[key]))
            elif elem[key] == '':
                obj[key].append(0)
            else:
                obj[key].append(elem[key])

    return obj


def list_find_index(name, code, list2):
    for index, elem in enumerate(list2):
        if elem["Country Name"] == name and elem["Country Code"] == code:
            return index

    return -1


def merge_lists(list1, list2):
    for elem in list1:
        index = list_find_index(elem["Country Name"], elem["Country Code"], list2)
        if index != -1:
            elem["GDP"] = elem["2017"]
            elem["Tariff"] = list2[index]["2017"]

    return list1


def draw_chart(data_list):
    data = parse_data(data_list)
    fig = px.scatter(data, x="GDP", y="Tariff", text="Country Name")
    fig.update_traces(textposition='top center')
    fig.update_layout(
        title_text="GDP and Tariff (10 random countries, 2017)"
    )
    fig.show()


def get_random_list(final_list, amount):
    limit = len(final_list) - 1
    new_list = []

    for x in range(amount - 1):
        new_list.append(final_list[random.randrange(0, limit)])

    return new_list


def main():
    args = parsing_arguments()
    gdp_list = csv_to_data(args.gdp_file)
    tariff_list = csv_to_data(args.tariff_file)

    final_list = merge_lists(gdp_list, tariff_list)
    data_list = get_random_list(final_list, 10)

    draw_chart(data_list)

if __name__ == "__main__":
    main()